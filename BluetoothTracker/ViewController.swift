//
//  ViewController.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/12/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit
import CoreBluetooth
import SVProgressHUD
import CoreLocation
import HDPhoneMonitor

class ViewController: UIViewController {

    @IBOutlet weak var imgViewDeviceImage: UIImageView!
    @IBOutlet weak var imgViewBatteryLevel: UIImageView!
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var lblDeviceSerialNumber: UILabel!
    @IBOutlet weak var btnUnlink: UIBarButtonItem!
    @IBOutlet weak var btnScan: UIBarButtonItem!
    @IBOutlet weak var btnShowDevice: ZFRippleButton!
    @IBOutlet weak var btnWhereDevice: UIButton!
    
    
    var connectingDevice: CBPeripheral?
    
    var unlinkAttemptTimer:NSTimer?
    var checkConnectionTimer:NSTimer!
    var userDefault:NSUserDefaults!
    var savedStrUUID:String?
    var isNewConnect = false
    var isUnlinked = false
    
    override func viewDidDisappear(animated: Bool) {
        userDefault.setValue(savedStrUUID, forKey: "savedUUID")
        userDefault.synchronize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        assignbackground()
        initialize()
    }
    
    func initialize() {
        userDefault = NSUserDefaults()
        savedStrUUID = userDefault.objectForKey("savedUUID") as? String
        
        BTManager.getInstance().centralManager.delegate = self
        LocationService.sharedInstance.delegate = self
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.appWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        setUpDivice()
    }
    
    func appWillEnterForeground(notification: NSNotification) {
        BTManager.getInstance().centralManager.delegate = self
        LocationService.sharedInstance.delegate = self
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            connectedDevice.delegate = self
            if BTManager.getInstance().isBluetoothEnabled  && BTManager.getInstance().isConnected{
                LocationService.sharedInstance.startUpdatingLocation()
            }
        }
    }
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.ScaleToFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        imageView.alpha = 0.8
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    func setUpDivice() {
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            print("Connected to Peripheral: \(connectedDevice)")
            self.savedStrUUID = connectedDevice.identifier.UUIDString
            connectedDevice.delegate = self
            BTManager.getInstance().isConnected = true
            
            displayDeviceInfomation()
            
            checkConnectionTimer = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: #selector(ViewController.checkConnection), userInfo: nil, repeats: true)
            checkDeviceServices()
            
            LocationService.sharedInstance.startUpdatingLocation()
            
            
            HDPhoneMonitor.sharedService.monitor()
        }
    }

    func displayDeviceInfomation() {
        let connectedDevice = BTManager.getInstance().connectedDevice
        // display device image
        imgViewDeviceImage.image = BTManager.imageByDeviceName((connectedDevice?.name)!)
        // display device name
        lblDeviceName.text = connectedDevice?.name
        
        btnShowDevice.hidden = false
        btnWhereDevice.hidden = false
    }
    
    func checkDeviceServices() {
        BTManager.getInstance().connectedDevice?.discoverServices(nil)
    }
    
    func checkConnection() {
        BTManager.getInstance().connectedDevice!.readRSSI()
    }
    
    func reconnectDevice() {
        if savedStrUUID != nil {
            let manager = BTManager.getInstance().centralManager
            manager.delegate = self
            let peripherals = manager!.retrievePeripheralsWithIdentifiers([NSUUID(UUIDString: savedStrUUID!)!])
            if peripherals.count > 0 {
                showLoadingProgess("Reconnecting")
                manager!.connectPeripheral(peripherals[0], options: nil)
                self.connectingDevice = peripherals[0]
            }
        }
    }
    
    func getBatteryLevelFromCharacteristic(characteristic :CBCharacteristic) {
        let data = characteristic.value!
        let resultArray = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(data.bytes), count: data.length))
        imgViewBatteryLevel.image = BTManager.batteryImageByPercentage(resultArray[0])
        //lblBatteryLevel.text = "\(resultArray[0])%"
    }
    
    func getSerialNumberFromCharacteristic(characteristic: CBCharacteristic) {
        let serialNumber = NSString(data: characteristic.value!, encoding: NSUTF8StringEncoding)
        lblDeviceSerialNumber.text = serialNumber as? String
    }

    func playLed() {
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            let data = NSData(bytes: BLEPlayLedCommand, length: BLEPlayLedCommand.count)
            connectedDevice.writeValue(data, forCharacteristic: BTManager.getInstance().ledPlayerCharacteristic!, type: CBCharacteristicWriteType.WithResponse)
        }
    }
    
    @IBAction func onUnlinkPressed(sender: AnyObject) {
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            let alert = UIAlertController(title: "Do you want to unlink this device?", message: nil, preferredStyle: .Alert)
            let actionYes = UIAlertAction(title: "Yes", style: .Destructive) { (action) in
                self.isNewConnect = false
                self.isUnlinked = true
                self.unlinkDevice(connectedDevice)
            }
            let actionNo = UIAlertAction(title: "No", style: .Cancel, handler: nil)
            alert.addAction(actionYes)
            alert.addAction(actionNo)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func unlinkDevice(connectedDevice: CBPeripheral) {
        showLoadingProgess("Unlinking device")
        print("Unlinking device")
        unlinkAttemptTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(ViewController.timeoutDeviceUnlinkAttempt), userInfo: nil, repeats: false)
        BTManager.getInstance().centralManager!.cancelPeripheralConnection(connectedDevice)
    }
    
    func timeoutDeviceUnlinkAttempt() {
        unlinkFailure()
        hideLoadingProgess()
        unlinkAttemptTimer?.invalidate()
    }
    
    func unlinkFailure() {
        print("Can not unlink")
        let alert = UIAlertController(title: "Can not unlink device!", message: nil, preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alert.addAction(actionOk)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func onScanPressed(sender: AnyObject) {
        if BTManager.getInstance().isBluetoothEnabled {
            if let connectedDevice = BTManager.getInstance().connectedDevice {
                let alert = UIAlertController(title: "If you add new device, the current connection will be lost!", message: nil, preferredStyle: .Alert)
                let actionOk = UIAlertAction(title: "OK", style: .Destructive)  {action -> Void in
                    self.isNewConnect = true
                    self.unlinkDevice(connectedDevice)
                }
                let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                alert.addAction(actionOk)
                alert.addAction(actionCancel)
                presentViewController(alert, animated: true, completion: nil)
            }
            else {
                performSegueWithIdentifier("segueScan", sender: nil)
            }
        }
        else {
            let alert = UIAlertController(title: "Please turn on your bluetooth!", message: nil, preferredStyle: .Alert)
            let actionOk = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alert.addAction(actionOk)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func showLoadingProgess(text: String) {
        SVProgressHUD.showWithStatus(text)
        btnScan.enabled = false
        btnUnlink.enabled = false
        btnShowDevice.enabled = false
        btnWhereDevice.enabled = false
    }
    
    func hideLoadingProgess() {
        SVProgressHUD.dismiss()
        btnUnlink.enabled = true
        btnScan.enabled = true
        btnShowDevice.enabled = true
        btnWhereDevice.enabled = true
    }
    
    func removeDeviceInfo() {
        imgViewDeviceImage.image = BTManager.imageByDeviceName("")
        imgViewBatteryLevel.image = BTManager.batteryImageByPercentage(0)
        lblDeviceName.text = ""
        lblDeviceSerialNumber.text = ""
        btnShowDevice.hidden = true
        btnWhereDevice.hidden = true
        savedStrUUID = nil
        userDefault.setValue(savedStrUUID, forKey: "savedUUID")
        userDefault.setValue(nil, forKey: "lastReceivedHeartBeat")
        userDefault.setValue(nil, forKey: "lastLatitude")
        userDefault.setValue(nil, forKey: "lastLongitude")
        userDefault.synchronize()
    }
    
    @IBAction func onShowMyButtonPressed(sender: AnyObject) {
        if let _ = BTManager.getInstance().connectedDevice {
            playLed()
        }
        else {
            let alert = UIAlertController(title: "No device connected", message: nil, preferredStyle: .Alert)
            let actionOk = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alert.addAction(actionOk)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
}

extension ViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        switch central.state {
        case .PoweredOff:
            BTManager.getInstance().isBluetoothEnabled = false
            BTManager.getInstance().isConnected = false
            LocationService.sharedInstance.stopUpdatingLocation()
            HDPhoneMonitor.sharedService.monitor()
            HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
        case .PoweredOn:
            BTManager.getInstance().isBluetoothEnabled = true
            self.reconnectDevice()
        default:
            BTManager.getInstance().isBluetoothEnabled = false
            BTManager.getInstance().isConnected = false
            HDPhoneMonitor.sharedService.monitor()
            HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        if self.connectingDevice == peripheral {
            hideLoadingProgess()
            BTManager.getInstance().connectedDevice = peripheral
            BTManager.getInstance().isConnected = true
            setUpDivice()
            HDPhoneMonitor.sharedService.monitor()
            HDPhoneMonitor.sharedService.deviceDidConnect((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
        }
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("Disconnected device: \(peripheral)")
        BTManager.getInstance().isConnected = false
        LocationService.sharedInstance.stopUpdatingLocation()
        HDPhoneMonitor.sharedService.monitor()
        HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
        
        if isNewConnect || isUnlinked {
            // remove device info from view
            self.removeDeviceInfo()
            hideLoadingProgess()
            BTManager.getInstance().connectedDevice = nil
            unlinkAttemptTimer?.invalidate()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let unlinkInstructionViewController = storyboard.instantiateViewControllerWithIdentifier("unlinkInstructionViewController") as! UnlinkInstructionViewController
            navigationController?.pushViewController(unlinkInstructionViewController, animated: true)
            
        }
        else {
            print("Lost connection")
            let alert = UIAlertController(title: "Lost connection to your device!", message: nil, preferredStyle: .Alert)
            let actionOk = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alert.addAction(actionOk)
            presentViewController(alert, animated: true, completion: nil)
            reconnectDevice()
        }
    }
}

extension ViewController: CBPeripheralDelegate {
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        for service in peripheral.services! {
            //print("Discovered service: \(service.UUID)")
            peripheral.discoverCharacteristics(nil, forService: service)
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        if service.UUID.isEqual(CBUUID(string: BLEService.Battery.rawValue)) {
            for char in service.characteristics! {
                if char.UUID.isEqual(CBUUID(string: BLECharacteristic.BatteryLevel.rawValue)) {
                    //print("Found battery level characteristic")
                    peripheral.readValueForCharacteristic(char)
                }
            }
        }
        else if service.UUID.isEqual(CBUUID(string: BLEService.DeviceInformation.rawValue)) {
            for char in service.characteristics! {
                if char.UUID.isEqual(CBUUID(string: BLECharacteristic.SerialNumber.rawValue)) {
                    //print("Found device serial number characteristic")
                    peripheral.readValueForCharacteristic(char)
                }
            }
        }
        else if service.UUID.isEqual(CBUUID(string: BLEService.Command.rawValue)) {
            for char in service.characteristics! {
                if char.UUID.isEqual(CBUUID(string: BLECharacteristic.LedPlayer.rawValue)) {
                    BTManager.getInstance().ledPlayerCharacteristic = char
                    peripheral.setNotifyValue(true, forCharacteristic: char)
                }
                else if char.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatCommandReceiver.rawValue)) {
                    //BTManager.getInstance().heartBeatCommandReceiverCharacteristic = char
                    //peripheral.setNotifyValue(true, forCharacteristic: char)
                }
                else if char.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatSender.rawValue)) {
                    //BTManager.getInstance().heartBeatSenderCharacteristic = char
                    //peripheral.setNotifyValue(true, forCharacteristic: char)
                }
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.BatteryLevel.rawValue)) {
            self.getBatteryLevelFromCharacteristic(characteristic)
        }
        else if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.SerialNumber.rawValue)) {
            self.getSerialNumberFromCharacteristic(characteristic)
        }
        else if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatCommandReceiver.rawValue)) {
            //print("received command")
        }
        else if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatSender.rawValue)) {
            //print("received heartbeat")
            //self.receiveHeartBeat(characteristic)
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        //print(characteristic)
        if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.LedPlayer.rawValue)) {
            self.playLed()
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if error != nil {
            //print("Characteristic: \(characteristic)")
            //print("Cannot write value: \(error)")
        }
        else {
            //print("write value: \(characteristic)")
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: NSError?) {
        //print(NSDate())
        HDPhoneMonitor.sharedService.monitor()
        if error != nil {
            //print("Cannot read RSSI number: \(error)")
        } else {
            //print("RSSI: \(RSSI)")
        }
    }
}

extension ViewController: LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        print("Current location: \(currentLocation.coordinate.latitude) - \(currentLocation.coordinate.longitude)")
        userDefault.setValue(currentLocation.coordinate.latitude, forKey: "lastLatitude")
        userDefault.setValue(currentLocation.coordinate.longitude, forKey: "lastLongitude")
        userDefault.synchronize()
        HDPhoneMonitor.sharedService.monitor()
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        
    }
}