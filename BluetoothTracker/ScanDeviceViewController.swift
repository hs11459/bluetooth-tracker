//
//  ScanDeviceViewController.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/12/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit
import CoreBluetooth
import SVProgressHUD

struct PeripheralDevice {
    var peripheral: CBPeripheral
    var name: String
    var UUID: String
    var RSSI: Int
    var connectable = "No"
    
    init(peripheral: CBPeripheral, RSSI: Int, advertisementDictionary: NSDictionary) {
        self.peripheral = peripheral
        name = peripheral.name!
        UUID = peripheral.identifier.UUIDString
        self.RSSI = RSSI
        if let isConnectable = advertisementDictionary[CBAdvertisementDataIsConnectable] as? NSNumber {
            connectable = (isConnectable.boolValue) ? "Yes" : "No"
        }
    }
}


class ScanDeviceViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCancel: UIBarButtonItem!
    @IBOutlet weak var btnRefresh: UIBarButtonItem!
    
    var connectingDevice:CBPeripheral?
    
    var scanTimer: NSTimer?
    var connectionAttemptTimer: NSTimer?
    var isBluetoothEnabled = true
    var discoveredDeviceUUIDs = NSMutableOrderedSet()
    var discoveredDevices = [String: PeripheralDevice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        BTManager.getInstance().centralManager.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.appWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
    }
    
    func appWillEnterForeground(notification: NSNotification) {
        BTManager.getInstance().centralManager.delegate = self
    }

    override func viewDidAppear(animated: Bool) {
        if isBluetoothEnabled {
            startScanning()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        if #available(iOS 9.0, *) {
            if BTManager.getInstance().centralManager.isScanning {
                stopScanning()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func startScanning() {
        print("Start scanning")
        discoveredDeviceUUIDs.removeAllObjects()
        discoveredDevices.removeAll(keepCapacity: true)
        tableView.reloadData()
        BTManager.getInstance().centralManager.scanForPeripheralsWithServices(nil, options: nil)
        scanTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(ScanDeviceViewController.stopScanning), userInfo: nil, repeats: false)
    }
    
    func stopScanning() {
        print("Stopped scanning.")
        print("Found \(discoveredDevices.count) peripheral devices.")
        BTManager.getInstance().centralManager.stopScan()
        scanTimer?.invalidate()
    }
    
    func timeoutDeviceConnectionAttempt() {
        failToConnect()
        //print("Peripheral device connection attempt timed out.")
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            BTManager.getInstance().centralManager.cancelPeripheralConnection(connectedDevice)
        }
        connectionAttemptTimer?.invalidate()
    }
    
    func failToConnect() {
        hideLoadingProgress()
        let alert = UIAlertController(title: "Can't connect to this device!", message: nil, preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(actionOk)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func onRefreshPressed(sender: AnyObject) {
        if #available(iOS 9.0, *) {
            if BTManager.getInstance().centralManager.isScanning {
                stopScanning()
            }
        } else {
            // Fallback on earlier versions
        }
        startScanning()
    }
    
    func showLoadingProgress(text: String) {
        SVProgressHUD.showWithStatus(text)
        self.tableView.userInteractionEnabled = false
        self.btnCancel.enabled = false
        self.btnRefresh.enabled = false
    }
    
    func hideLoadingProgress() {
        SVProgressHUD.dismiss()
        self.tableView.userInteractionEnabled = true
        self.btnCancel.enabled = true
        self.btnRefresh.enabled = true
    }
    
    @IBAction func onCancelPressed(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension ScanDeviceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredDevices.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "What device are you setting up?"
        }
        return ""
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DeviceCell", forIndexPath: indexPath) as! DeviceTableViewCell
        
        if let discoveredUUID = discoveredDeviceUUIDs[indexPath.row] as? String {
            if let discoveredDevice = discoveredDevices[discoveredUUID] {
                if discoveredDevice.connectable == "No" {
                    cell.accessoryType = .None
                }
                cell.setupWithPeripheral(discoveredDevice)
            }
        }
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let selectedUUID = discoveredDeviceUUIDs[indexPath.row] as? String,
            selectedDevice = discoveredDevices[selectedUUID] {
            if selectedDevice.connectable == "Yes" {
                stopScanning()
                showLoadingProgress("Connecting")
                connectingDevice = selectedDevice.peripheral
                connectionAttemptTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(ScanDeviceViewController.timeoutDeviceConnectionAttempt), userInfo: nil, repeats: false)
                BTManager.getInstance().centralManager.connectPeripheral(connectingDevice!, options: nil)
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
    }
}

extension ScanDeviceViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch central.state {
        case .PoweredOn:
            isBluetoothEnabled = true
        case .PoweredOff:
            isBluetoothEnabled = false
            stopScanning()
        default:
            isBluetoothEnabled = false
        }
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        if peripheral.name != nil {
            discoveredDeviceUUIDs.addObject(peripheral.identifier.UUIDString)
            discoveredDevices[peripheral.identifier.UUIDString] = PeripheralDevice(peripheral: peripheral, RSSI: RSSI.integerValue, advertisementDictionary: advertisementData)
            tableView.reloadData()
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        if connectingDevice == peripheral {
            hideLoadingProgress()
            print("Peripheral device connected: \(peripheral.name ?? peripheral.identifier.UUIDString)")
            connectionAttemptTimer?.invalidate()
            BTManager.getInstance().connectedDevice = peripheral
            performSegueWithIdentifier("segueConnected", sender: nil)
        }
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        self.failToConnect()
    }
}