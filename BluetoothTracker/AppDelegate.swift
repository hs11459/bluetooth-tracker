//
//  AppDelegate.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/5/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit
import CoreData
import CoreBluetooth
import CoreLocation
import HDPhoneMonitor

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        HDPhoneMonitor.startService()
        HDPhoneMonitor.enableCloudStorage()
        return true
    }

        
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        setUpForBackgroundMode()
    }

    func setUpForBackgroundMode() {
        
        BTManager.getInstance().centralManager.delegate = self
        LocationService.sharedInstance.delegate = self
        
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            connectedDevice.delegate = self
            if BTManager.getInstance().isBluetoothEnabled  && BTManager.getInstance().isConnected {
                print("Setting up for background mode")
                
                connectedDevice.discoverServices(nil)
                LocationService.sharedInstance.startUpdatingLocation()
            }
        }
    }
    
    func reconnectDevice() {
        let manager = BTManager.getInstance().centralManager
        manager.delegate = self
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            connectedDevice.delegate = self
            if let savedStrUUID = connectedDevice.identifier.UUIDString as? String {
                let peripherals = manager!.retrievePeripheralsWithIdentifiers([NSUUID(UUIDString: savedStrUUID)!])
                if peripherals.count > 0 {
                    print("Reconnecting...")
                    manager!.connectPeripheral(peripherals[0], options: nil)
                }
            }
            
        }
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        // stop sending heartbeat 
        setUpForForegroundMode()
    }
    
    func setUpForForegroundMode() {
        if let connectedDevice = BTManager.getInstance().connectedDevice {
            print("Stop receiving heartbeat")
                        if let heartBeatSenderCharacteristic = BTManager.getInstance().heartBeatSenderCharacteristic {
                connectedDevice.setNotifyValue(false, forCharacteristic: heartBeatSenderCharacteristic)
                let userDefault = NSUserDefaults()
                let lastReceivedTime:String? = userDefault.stringForKey("lastReceivedHeartBeat")
                print("Last received heart beat: \(lastReceivedTime)")
                LocationService.sharedInstance.stopUpdatingLocation()
                let lastLatitude = userDefault.objectForKey("lastLatitude") as? Double
                let lastLongitude = userDefault.objectForKey("lastLongitude") as? Double
                print("Last location: \(lastLatitude) - \(lastLongitude)")
            }
        }

    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.dqhieu.BluetoothTracker" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("BluetoothTracker", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

extension AppDelegate: CBPeripheralDelegate {
    func peripheral(peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: NSError?) {
        if error != nil {
            print(error)
        }
        else {
            print("RSSI: \(RSSI)")
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, forService: service)
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        if service.UUID.isEqual(CBUUID(string: BLEService.Command.rawValue)) {
            for char in service.characteristics! {
                if char.UUID.isEqual(CBUUID(string: BLECharacteristic.LedPlayer.rawValue)) {
                    BTManager.getInstance().ledPlayerCharacteristic = char
                    peripheral.setNotifyValue(true, forCharacteristic: char)
                }
                else if char.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatCommandReceiver.rawValue)) {
                    BTManager.getInstance().heartBeatCommandReceiverCharacteristic = char
                    peripheral.setNotifyValue(true, forCharacteristic: char)
                }
                else if char.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatSender.rawValue)) {
                    BTManager.getInstance().heartBeatSenderCharacteristic = char
                    peripheral.setNotifyValue(true, forCharacteristic: char)
                }
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatCommandReceiver.rawValue)) {
            // send heart beat command
            let data = NSData(bytes: BLESendHeartBeatCommand, length: BLESendHeartBeatCommand.count)
            if let connectedDevice = BTManager.getInstance().connectedDevice, heartBeatCommandReceiverCharacteristic = BTManager.getInstance().heartBeatCommandReceiverCharacteristic {
                connectedDevice.writeValue(data, forCharacteristic: heartBeatCommandReceiverCharacteristic, type: CBCharacteristicWriteType.WithoutResponse)
            }
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            print("Sent heart beat command: \(dateFormatter.stringFromDate(NSDate()))")
            
        }
        else if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatSender.rawValue)) {
            
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        if characteristic.UUID.isEqual(CBUUID(string: BLECharacteristic.HeartBeatSender.rawValue)) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let lastReceivedTime = dateFormatter.stringFromDate(NSDate())
            print("Received heartbeat: \(lastReceivedTime)")
            let userDefault = NSUserDefaults()
            userDefault.setValue(lastReceivedTime, forKey: "lastReceivedHeartBeat")
            userDefault.synchronize()
            HDPhoneMonitor.sharedService.monitor()
        }
    }
}

extension AppDelegate: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        switch central.state {
        case .PoweredOff:
            // bluetooth powered off
            BTManager.getInstance().isBluetoothEnabled = false
            BTManager.getInstance().isConnected = false
            LocationService.sharedInstance.stopUpdatingLocation()
            HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
            print("bluetooth powered off")
        case .PoweredOn:
            // bluetooth powered on
            BTManager.getInstance().isBluetoothEnabled = true
            BTManager.getInstance().isConnected = true
            print("bluetooth powered on")
            self.reconnectDevice()
        default:
            BTManager.getInstance().isBluetoothEnabled = false
            BTManager.getInstance().isConnected = false
            HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
            return
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("Reconnected")
        BTManager.getInstance().connectedDevice = peripheral
        BTManager.getInstance().connectedDevice?.delegate = self
        BTManager.getInstance().isConnected = true
        peripheral.discoverServices(nil)
        LocationService.sharedInstance.startUpdatingLocation()
        HDPhoneMonitor.sharedService.deviceDidConnect((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        LocationService.sharedInstance.stopUpdatingLocation()
        BTManager.getInstance().isConnected = false
        HDPhoneMonitor.sharedService.deviceConnectionDidDrop((BTManager.getInstance().connectedDevice?.identifier.UUIDString)!)
        print("Lost connection: \(peripheral)")
        reconnectDevice()
    }
}

extension AppDelegate: LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        print("BG current location: \(currentLocation.coordinate.latitude) - \(currentLocation.coordinate.longitude)")
        let userDefault = NSUserDefaults()
        userDefault.setValue(currentLocation.coordinate.latitude, forKey: "lastLatitude")
        userDefault.setValue(currentLocation.coordinate.longitude, forKey: "lastLongitude")
        userDefault.synchronize()
        HDPhoneMonitor.sharedService.monitor()
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        
    }
}