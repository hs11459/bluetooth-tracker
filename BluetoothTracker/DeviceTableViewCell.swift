//
//  DeviceTableViewCell.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/12/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewDeviceImage: UIImageView!
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    func setupWithPeripheral(peripheral: PeripheralDevice) {
        lblDistance.text = BTManager.distanceByRSSI(peripheral.RSSI)
        imgViewDeviceImage.image = BTManager.imageByDeviceName(peripheral.name)
        lblDeviceName.text = peripheral.name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
