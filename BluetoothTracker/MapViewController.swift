//
//  MapViewController.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/14/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var userDefault:NSUserDefaults!
    var latitude:Double!
    var longitude:Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    func initialize() {
        userDefault = NSUserDefaults()
        latitude = userDefault.objectForKey("lastLatitude") as! Double
        longitude = userDefault.objectForKey("lastLongitude") as! Double
        
        let latitudeDelta:CLLocationDegrees = 0.01
        let longtitudeDelta:CLLocationDegrees = 0.01
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latitudeDelta, longtitudeDelta)
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.title = "Your device was here"
        annotation.coordinate = location
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude), completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print("Address: \(placeMark.addressDictionary)")
            
            if let formattedAddressLines = placeMark.addressDictionary!["FormattedAddressLines"] as? NSArray {
                var fullAddress:String = ""
                for value in formattedAddressLines {
                    fullAddress += "\(value), "
                }
                if fullAddress != "" {
                    fullAddress.removeAtIndex(fullAddress.endIndex.predecessor())
                    fullAddress.removeAtIndex(fullAddress.endIndex.predecessor())
                    annotation.subtitle = fullAddress
                }
            }
            
            self.mapView.addAnnotation(annotation)
        })
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
