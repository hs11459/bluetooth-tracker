//
//  UnlinkInstructionViewController.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/13/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit

class UnlinkInstructionViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var btnNext: ZFRippleButton!
    var unlinkInstructionPageViewController: UnlinkInstructionPageViewController? {
        didSet {
            unlinkInstructionPageViewController?.unlinkInstructionDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.addTarget(self, action: #selector(UnlinkInstructionViewController.didChangePageControlValue), forControlEvents: .ValueChanged)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let tutorialPageViewController = segue.destinationViewController as? UnlinkInstructionPageViewController {
            self.unlinkInstructionPageViewController = tutorialPageViewController
        }
    }
    
    @IBAction func didTapNextButton(sender: UIButton) {
        //unlinkInstructionPageViewController?.scrollToNextViewController()
        let index = pageControl.currentPage + 1
        if index <= 2 {
            unlinkInstructionPageViewController?.scrollToViewController(index: index)
        }
        else {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        unlinkInstructionPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }

}

extension UnlinkInstructionViewController: UnlinkInstructionPageViewControllerDeletage {
    
    func unlinkInstrcutionPageViewController(unlinkInstrcutionPageViewController: UnlinkInstructionPageViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    
    func unlinkInstrcutionPageViewController(unlinkInstrcutionPageViewController: UnlinkInstructionPageViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        if index >= 2 {
            btnNext.titleLabel?.text = "OK"
        }
        else {
            btnNext.titleLabel?.text = "NEXT"
        }
    }
    
}