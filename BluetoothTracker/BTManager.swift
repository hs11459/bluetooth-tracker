//
//  BTManager.swift
//  BluetoothTracker
//
//  Created by Dinh Quang Hieu on 7/12/16.
//  Copyright © 2016 Dinh Quang Hieu. All rights reserved.
//

import UIKit
import CoreBluetooth

enum BLEService: String {
    case Battery                = "0x180F"
    case Location               = "0x1819"
    case DeviceInformation      = "0x180A"
    case Command                = "3DDA0001-957F-7D4A-34A6-74696673696D"
}

enum BLECharacteristic: String {
    
    case Locaiton                   = "0x2A67"
    case BatteryLevel               = "0x2A19"
    case ManufacturerName           = "0x2A29"
    case SerialNumber               = "0x2A25"
    case LedPlayer                    = "3DDA0002-957F-7D4A-34A6-74696673696D"
    case HeartBeatCommandReceiver   = "3DDA0003-957F-7D4A-34A6-74696673696D"
    case HeartBeatSender            = "3DDA0004-957F-7D4A-34A6-74696673696D"
}

let BLEPlayLedCommand:[UInt8] = [0x02, 0xF1, 0x05]
let BLESendHeartBeatCommand:[UInt8] = [0x01, 0xF0, 0x8E]

class BTManager: NSObject {

    static var manager:BTManager?
    var inited = false
    
    var centralManager:CBCentralManager!
    var connectedDevice:CBPeripheral?
    var ledPlayerCharacteristic:CBCharacteristic?
    var heartBeatSenderCharacteristic:CBCharacteristic?
    var heartBeatCommandReceiverCharacteristic:CBCharacteristic?
    
    var isBluetoothEnabled = false
    var isConnected = false
    
    func initialize() {
        if inited {
            return
        }
        inited = true
        connectedDevice = nil
        centralManager = CBCentralManager(delegate: nil, queue: dispatch_get_main_queue())
    }
    
    static func getInstance() -> BTManager {
        if manager == nil {
            manager = BTManager()
            manager!.initialize()
        }
        return manager!
    }
    
    static func distanceByRSSI(RSSI :Int)->String {
        if RSSI > -66 {
            return "VERY CLOSE"
        }
        else if RSSI > -90 {
            return "NEARBY"
        }
        return "FAR"
    }
    
    static func imageByDeviceName(name: String) -> UIImage {
        if name.lowercaseString.containsString("shine2") || name.lowercaseString.containsString("shine 2") {
            return UIImage(named: "misfit_shine2")!
        }
        else if name.lowercaseString.containsString("shine") {
            return UIImage(named: "misfit_shine")!
        }
        else if name.lowercaseString.containsString("ray") {
            return UIImage(named: "misfit_ray")!
        }
        else if name.lowercaseString.containsString("flash") {
            return UIImage(named: "misfit_flash")!
        }
        return UIImage(named: "unknow_device")!
    }

    static func batteryImageByPercentage(percentage: UInt8) -> UIImage {
        if percentage > 95 {
            return UIImage(named: "battery_full_level")!
        }
        else if percentage > 75 {
            return UIImage(named: "battery_high_level")!
        }
        else if percentage > 50 {
            return UIImage(named: "battery_medium_level")!
        }
        else if percentage > 25 {
            return UIImage(named: "battery_low_level")!
        }
        return UIImage(CIImage: CIImage(color: CIColor(red: 0, green: 0, blue: 0, alpha: 0)))
    }
}
