# Bluetooth Tracker

Bluetooth Tracker is a tracker of your bluetooth device.

## Features

* Connect with your device via bluetooth
* Tracking your device anytime and anywhere
* Show last location of your device if connection lost

## Todo list
* Graph for battery log
